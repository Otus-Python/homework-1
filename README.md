# Advanced Python. Week 1

## Привет 

## 1. Анализатор логов

Лежит в папке `log_analyzer`, работает как под вторым питоном, так и под третьим

Запускается как 

```
$: cd log_analyzer
$: python log_analyzer.py [-h] [--config CONFIG] 
```

Тесты (они синтетические чуть менее чем полностью): 

```
$: python -m unittest -v test_log_analyzer 
```

или

```
$: python test_log_analyzer.py
```

## 2. Покер и декораторы

Декораторы я сделал, а решение покера нашел в интернете и попытался разобраться что там происходит.
Файлы лежат в корне проекта
