#!/usr/bin/env python
# -*- coding: utf-8 -*-


# log_format ui_short '$remote_addr  $remote_user $http_x_real_ip [$time_local] "$request" '
#                     '$status $body_bytes_sent "$http_referer" '
#                     '"$http_user_agent" "$http_x_forwarded_for" "$http_X_REQUEST_ID" "$http_X_RB_USER" '
#                     '$request_time';


from __future__ import division
import argparse
import functools
import io
import gzip
import json
import logging
import os
import string
from collections import namedtuple
from datetime import datetime

default_config = {
    "REPORT_SIZE": 1000,
    "REPORT_DIR": "./reports",
    "LOG_DIR": "./log",
    "MONITORING_LOG": None,
    "ERRORS_RATE_LIMIT": 0.1,
}


def set_monitoring_config(log_file_path=None):
    """
    simple wrapper for logging configuration

    :param log_file_path:
    :return:
    """
    LOG_FORMAT = '[%(asctime)s] %(levelname).1s %(message)s'
    logging.basicConfig(
        format=LOG_FORMAT,
        filename=log_file_path,
        level=logging.DEBUG,
        datefmt='%Y-%m-%d %H:%M:%S'
    )


def logger(func):
    """
    so we can log each step of our process

    :param func:
    :return:
    """

    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        try:
            logging.info('{} started'.format(func.__name__))
            result = func(*args, **kwargs)
            return result
        except (KeyboardInterrupt, Exception):
            logging.exception('An error occurred in {}'.format(func.__name__), exc_info=True)
            raise
        finally:
            logging.info('{} finished'.format(func.__name__))
    return wrapper


def get_report_path(report_dir, date):
    filename = 'report-{}.html'.format(date.strftime('%Y.%m.%d'))
    return os.path.join(report_dir, filename)


@logger
def report_exists(report_dir, date):
    """
    check if report file already exists

    :param report_dir: str
    :param date: datetime
    :return: bool
    """

    path = get_report_path(report_dir, date)
    return os.path.isfile(path)


@logger
def render_report(data):
    """
    insert json with report data in template

    :param data: list
    :return: str
    """

    table_json = json.dumps(data, sort_keys=True)
    with open('report.html') as html:
        report_template = string.Template(html.read())
        return report_template.safe_substitute(table_json=table_json)


@logger
def save_report(report, log_date, report_dir):
    """
    save report file

    :param report:
    :param log_date:
    :param report_dir:
    :return:
    """

    if not os.path.isdir(report_dir):
        os.mkdir(report_dir, 0o755)

    path = get_report_path(report_dir, log_date)
    with open(path, 'w') as report_file:
        report_file.write(report)


# define namedtuple for logfile description
LogFile = namedtuple('LogFile', "path date ext")


@logger
def get_latest_log(log_dir):
    """
    searches for latest log file

    :param log_dir: str
    :return: LogFile|None
    """

    # we could to use the glob module, but we have recommendation to avoid it in the homework
    files = [
        file for file in os.listdir(log_dir)
        if file.endswith(('.log', '.gz')) and file.startswith('nginx-access-ui.log-')
    ]
    if files:
        filename = max(files)
        path = os.path.join(log_dir, filename)
        _, ext = os.path.splitext(path)

        DATE = slice(20, 28)
        date = datetime.strptime(filename[DATE], '%Y%m%d')

        return LogFile(path, date, ext)
    else:
        return None


def get_args():
    """
    parses and returns command line args

    :return:
    """

    parser = argparse.ArgumentParser(description='Parse nginx logs')
    parser.add_argument('--config', default=None, help='path to custom config')
    return parser.parse_args()


def get_config(default_config, path=None):
    """
    merges default config and custom config

    :param default_config:dict
    :param path:string
    :return: dict
    """
    custom_config = {}
    if path is not None:
        with open(path) as f:
            custom_config = json.load(f)
    result = default_config.copy()
    result.update(custom_config)
    return result


def median(numbers):
    """
    returns median for sequence of numbers

    >>> median([1,2])
    1.5
    >>> median([2, 1, 3])
    2
    >>> median([0, 7 , 2 , 3 , 6 , 5 , 4 , 1 , 8])
    4
    >>> median([0, 7 , 2 , 3 , 6 , 5 , 4 , 1 , 8, 8])
    4.5

    :param numbers:
    :return:
    """
    sorted_, count = sorted(numbers), len(numbers)
    middle = count // 2
    return (sorted_[middle - 1] + sorted_[middle]) / 2 if count % 2 == 0 else sorted_[middle]


def parse_log(log):
    ALLOWED_METHODS = {'GET', 'POST', 'PUT', 'DELETE', 'PATCH'}
    METHOD_INDEX, URL_INDEX, PROTOCOL_INDEX, TIME_INDEX = 5, 6, 7, -1
    fopen = gzip.open if log.ext == '.gz' else io.open
    with fopen(log.path) as logs:
        for line in logs:
            # костыль для поддержки питона 2 и 3
            if not isinstance(line, str):
                line = line.decode()
            # я начал было писать регулярку, но потом подумал: а зачем
            parts = line.split()
            try:
                method, protocol = parts[METHOD_INDEX].strip('"').upper(), parts[PROTOCOL_INDEX].strip('"').upper()
                if method not in ALLOWED_METHODS or (protocol.startswith(r'HTTP/') is False):
                    raise ValueError
                yield parts[URL_INDEX], float(parts[TIME_INDEX])
            except ValueError:
                yield None, None


@logger
def process_log(log, errors_rate_limit):
    failed_lines = 0
    requests_times = {}
    aggregate = {
        'requests': 0,
        'total_time': 0,
    }

    for url, time in parse_log(log):
        if url is None:
            failed_lines += 1
            continue
        requests_times.setdefault(url, []).append(time)
        aggregate['requests'] += 1
        aggregate['total_time'] += time

    def reducer(acc, kv):
        url, times = kv
        acc.append({
            'url': url,
            'count': len(times),
            'count_perc': round(len(times) / aggregate['requests'] * 100, 3),
            'time_sum': round(sum(times), 3),
            'time_perc': round(sum(times) / aggregate['total_time'] * 100, 3),
            'time_avg': round(sum(times) / len(times), 3),
            'time_max': round(max(times), 3),
            'time_med': round(median(times), 3),
        })
        return acc

    total_lines = failed_lines + aggregate['requests']
    errors_rate = failed_lines / total_lines

    if errors_rate > errors_rate_limit:
        template = (
            'Too many errors while parsing. '
            'Parsed {total:0,d} lines, {errors:0,d} ({errors_rate:.2f}%) errors occurred, '
            'errors limit {errors_rate_limit:.2f}%'
        )
        logging.error(template.format(
            total=total_lines,
            errors=failed_lines,
            errors_rate=errors_rate * 100,
            errors_rate_limit=errors_rate_limit * 100
        ))
        raise ValueError('Too many fails')

    return functools.reduce(reducer, requests_times.items(), [])


def main():
    log = get_latest_log(config['LOG_DIR'])
    if log is None:
        logging.info('Nothing to parse')
        return

    if report_exists(config['REPORT_DIR'], log.date):
        logging.info('Report for {} already exists'.format(log.path))
        return

    try:
        data = process_log(log, config.get('ERRORS_RATE_LIMIT', 0.1))
        top = sorted(data, key=lambda x: x['time_sum'], reverse=True)[:config.get('REPORT_SIZE', 1000)]
        report = render_report(top)
        save_report(report, log.date, config['REPORT_DIR'])
    except ValueError:
        logging.info('Processing failed')


if __name__ == "__main__":

    args = get_args()
    try:
        config = get_config(default_config, args.config)
    except (ValueError, IOError):
        # can't use logger here, logger settings are stored in the config, so just printing
        print("Can't read config file. Check if it exists and contains valid json\n")
        raise  # just raise same exception again, so user will be able to see traceback in terminal
    set_monitoring_config(config.get('MONITORING_LOG'))

    main()
