#!/usr/bin/env python
# -*- coding: utf-8 -*-

import io
import unittest
from datetime import datetime
from unittest import TestCase

import log_analyzer


class TestLogAnalyzer(TestCase):
    """
    пардон за тупые тесты, правда не знаю, что в этом модуле покрывать
    """

    def test_get_report_path(self):
        cases = [
            ('.', datetime(2018, 8, 21), './report-2018.08.21.html'),
            ('.', datetime(2014, 2, 28), './report-2014.02.28.html')
        ]
        for dir_, date, expected in cases:
            # with self.subTest(dir=dir, date=date, expected=expected): в 2,7 нет subTest :((
            self.assertEqual(log_analyzer.get_report_path(dir_, date), expected)

    def test_report_exists(self):
        """
        ну тут в общем то нечего покрывать тестами.
        первая функция уже покрыта, вторая библиотечная

        хотел замокать os.path.isfile, чтобы показать, что могу,
        но оказалось что в 2.7 нет модуля unittest.mock

        :return:
        """

    def test_report_render(self):
        """
        тоже странный тест, просто чтобы был.
        а чтобы он не ломался добавил в json.dumps сортировку по ключу

        :return:
        """

        data = [{"count": 2767, "count_perc": 0.106, "time_avg": 62.995, "time_max": 9843.569, "time_med": 60.073,
                 "time_perc": 9.043, "time_sum": 174306.352, "url": "/api/v2/internal/html5/phantomjs/queue/?wait=1m"},
                {"count": 1410, "count_perc": 0.054, "time_avg": 67.106, "time_max": 9853.373, "time_med": 60.124,
                 "time_perc": 4.909, "time_sum": 94618.864,
                 "url": "/api/v2/internal/gpmd_plan_report/queue/?wait=1m&worker=5"}]

        with io.open('./tests_static/test_report.html', encoding='utf-8') as f:
            expected = f.read()

        rendered = log_analyzer.render_report(data)
        self.assertEqual(expected, rendered, 'Rendered content are not equal to file')

    def test_save_report(self):
        """
        тест объявлен чтобы задать вопрос:
        настолько правильно в тестах по-честному писать файлы на диск?
        какие best practice для написания таких тестов?

        :return:
        """

    def test_median(self):
        """
        а еще к ней написан doctest прямо в модуле

        :return:
        """

        cases = [
            ([1, 2], 1.5),
            ([2, 1, 3], 2),
            ([0, 7, 2, 3, 6, 5, 4, 1, 8], 4),
            ([0, 7, 2, 3, 6, 5, 4, 1, 8, 8], 4.5)
        ]
        for numbers, median in cases:
            self.assertEqual(log_analyzer.median(numbers), median)


if __name__ == "__main__":
    unittest.main(verbosity=2)
