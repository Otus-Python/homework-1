#!/usr/bin/env python
# -*- coding: utf-8 -*-

import itertools

# -----------------
# Реализуйте функцию best_hand, которая принимает на вход
# покерную "руку" (hand) из 7ми карт и возвращает лучшую
# (относительно значения, возвращаемого hand_rank)
# "руку" из 5ти карт. У каждой карты есть масть(suit) и
# ранг(rank)
# Масти: трефы(clubs, C), пики(spades, S), червы(hearts, H), бубны(diamonds, D)
# Ранги: 2, 3, 4, 5, 6, 7, 8, 9, 10 (ten, T), валет (jack, J), дама (queen, Q), король (king, K), туз (ace, A)
# Например: AS - туз пик (ace of spades), TH - дестяка черв (ten of hearts), 3C - тройка треф (three of clubs)

# Задание со *
# Реализуйте функцию best_wild_hand, которая принимает на вход
# покерную "руку" (hand) из 7ми карт и возвращает лучшую
# (относительно значения, возвращаемого hand_rank)
# "руку" из 5ти карт. Кроме прочего в данном варианте "рука"
# может включать джокера. Джокеры могут заменить карту любой
# масти и ранга того же цвета, в колоде два джокерва.
# Черный джокер '?B' может быть использован в качестве треф
# или пик любого ранга, красный джокер '?R' - в качестве черв и бубен
# любого ранга.

# Одна функция уже реализована, сигнатуры и описания других даны.
# Вам наверняка пригодится itertools.
# Можно свободно определять свои функции и т.п.
# -----------------


def hand_rank(hand):
    """Возвращает значение определяющее ранг 'руки'"""
    ranks = card_ranks(hand)
    if straight(ranks) and flush(hand):
        return (8, max(ranks))
    elif kind(4, ranks):
        return (7, kind(4, ranks), kind(1, ranks))
    elif kind(3, ranks) and kind(2, ranks):
        return (6, kind(3, ranks), kind(2, ranks))
    elif flush(hand):
        return (5, ranks)
    elif straight(ranks):
        return (4, max(ranks))
    elif kind(3, ranks):
        return (3, kind(3, ranks), ranks)
    elif two_pair(ranks):
        return (2, two_pair(ranks), ranks)
    elif kind(2, ranks):
        return (1, kind(2, ranks), ranks)
    else:
        return (0, ranks)


def card_ranks(hand):
    """Возвращает список рангов (его числовой эквивалент),
    отсортированный от большего к меньшему"""
    ranks_list = ['2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K', 'A']
    ranks = [ranks_list.index(x[0]) for x in hand]
    ranks.sort(reverse=True)
    return ranks


def flush(hand):
    """Возвращает True, если все карты одной масти"""
    suits = [x[1] for x in hand]
    return len(set(suits)) == 1


def straight(ranks):
    """Возвращает True, если отсортированные ранги формируют последовательность 5ти,
    где у 5ти карт ранги идут по порядку (стрит)"""
    return ranks[0] - ranks[-1] == 4


def kind(n, ranks):
    """Возвращает первый ранг, который n раз встречается в данной руке.
    Возвращает None, если ничего не найдено"""
    rank_n = None
    for rank in set(ranks):
        if ranks.count(rank) == n:
            rank_n = rank
    return rank_n


def two_pair(ranks):
    """Если есть две пары, то возврщает два соответствующих ранга,
    иначе возвращает None"""
    rank_n1 = rank_n2 = None
    for rank in set(ranks):
        if ranks.count(rank) == 2:
            if not rank_n1:
                rank_n1 = rank
            else:
                rank_n2 = rank
    if rank_n1 and rank_n2:
        return (rank_n1, rank_n2)
    else:
        return


def all_hands(hand):
    """

    :param hand:
    :return:
    """
    hands = []
    for el in itertools.combinations(hand, 5):
        hands.append((hand_rank(el), el))
    return hands


def best_hand(hand):
    """Из "руки" в 7 карт возвращает лучшую "руку" в 5 карт """
    hands = all_hands(hand)
    hands = sorted(hands, key=lambda x: (x[0]),  reverse=True)
    return hands[0][1]


def all_hands_with_J(hand7, joker_dict, hands):
    if '?B' in hand7:
        for j in joker_dict['B']:
            hand7_new = hand7[:]
            hand7_new[hand7_new.index('?B')] = j
            all_hands_with_J(hand7_new, joker_dict, hands)
    elif '?R' in hand7:
        for j in joker_dict['R']:
            hand7_new = hand7[:]
            hand7_new[hand7_new.index('?R')] = j
            all_hands_with_J(hand7_new, joker_dict, hands)
    else:
        hands.extend(all_hands(hand7))
    return hands


def best_wild_hand(hand7):
    """best_hand но с джокерами"""
    suits = {'B': ('S', 'C'), 'R': ('H', 'D')}
    ranks_list = ['2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K', 'A']
    joker_dict = {}
    for color in suits:
        joker_list = []
        for suit in suits[color]:
            for r in ranks_list:
                joker_list.append(r+suit)
        joker_dict[color] = joker_list
    hands = []
    hands = all_hands_with_J(hand7, joker_dict, hands)
    hands = sorted(hands, key=lambda x: (x[0]),  reverse=True)
    return hands[0][1]


def test_best_hand():
    print("test_best_hand...")
    assert (sorted(best_hand("6C 7C 8C 9C TC 5C JS".split()))
            == ['6C', '7C', '8C', '9C', 'TC'])
    assert (sorted(best_hand("TD TC TH 7C 7D 8C 8S".split()))
            == ['8C', '8S', 'TC', 'TD', 'TH'])
    assert (sorted(best_hand("JD TC TH 7C 7D 7S 7H".split()))
            == ['7C', '7D', '7H', '7S', 'JD'])
    print('OK')


def test_best_wild_hand():
    print("test_best_wild_hand...")
    assert (sorted(best_wild_hand("6C 7C 8C 9C TC 5C ?B".split()))
            == ['7C', '8C', '9C', 'JC', 'TC'])
    assert (sorted(best_wild_hand("TD TC 5H 5C 7C ?R ?B".split()))
            == ['7C', 'TC', 'TD', 'TH', 'TS'])
    assert (sorted(best_wild_hand("JD TC TH 7C 7D 7S 7H".split()))
            == ['7C', '7D', '7H', '7S', 'JD'])
    print('OK')

if __name__ == '__main__':
    test_best_hand()
    test_best_wild_hand()